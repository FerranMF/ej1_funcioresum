
import java.io.*;
import java.util.*;
import org.apache.commons.codec.digest.Crypt;

public class GestionarUsers {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce tu nombre de usuario:");
        String username = scanner.nextLine();
        System.out.println("Introduce tu contraseña:");
        String password = scanner.nextLine();

        if (checkCredentials(username, password)) {
            System.out.println("¡Bienvenido " + username + "!");
            System.out.println("¿Quieres cambiar tu contraseña? (s/n)");
            String changePassword = scanner.nextLine();
            if (changePassword.equalsIgnoreCase("s")) {
                System.out.println("Introduce tu nueva contraseña:");
                String newPassword = scanner.nextLine();
                changePassword(username, newPassword);
                System.out.println("Tu contraseña ha sido cambiada.");
            }
        } else {
            System.out.println("Nombre de usuario o contraseña incorrectos.");
        }
        scanner.close();
    }

    private static boolean checkCredentials(String username, String password) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("/etc/shadow"));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(":");
            if (parts[0].equals(username)) {
                String[] hashParts = parts[1].split("\\$");
                String salt = hashParts[2];
                String hash = Crypt.crypt(password, "$6$" + salt);
                return parts[1].equals(hash);
            }
        }
        reader.close();
        return false;
    }

    private static void changePassword(String username, String newPassword) throws IOException {
        File tempFile = File.createTempFile("shadow", null);
        BufferedReader reader = new BufferedReader(new FileReader("/etc/shadow"));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(":");
            if (parts.length >= 7 && parts[0].equals(username)) {
                String[] hashParts = parts[1].split("\\$");
                String salt = hashParts[2];
                String newHash = Crypt.crypt(newPassword, "$6$" + salt);
                line = username + ":" + newHash + ":" + parts[2] + ":" + parts[3] + ":" + parts[4] + ":" + parts[5] + ":" + parts[6];
                if (parts.length > 7) {
                    for (int i = 7; i < parts.length; i++) {
                        line += ":" + parts[i];
                    }
                }
            }
            writer.write(line);
            writer.newLine();
        }
        reader.close();
        writer.close();
        tempFile.renameTo(new File("/etc/shadow"));
    }

}
